Jinni
=====

This is a dataset for Guildwars 2 The Shatterer Event, used in combination with the `bronk <https://gitlab.com/networkjanitor/bronk>`_ module.

There are also static html pages generated with `canni <https://gitlab.com/networkjanitor/canni>`_: https://networkjanitor.gitlab.io/jinni


How2Use
-------

#. clone repository: `git clone <repository>`
#. create and activate virtual environment (optional, but recommended): `python3 -m venv venv && source venv/bin/activate`
#. install requirements (bronk): `pip install -r requirements.txt`
#. run it: `python3 -m bronk shatterer`

from configurations.baseconf import BaseConf
from settings import Settings

class Conf(BaseConf):
    name = 'Orga'

    def __init__(self):
        super().__init__()
        self._load_csv(Settings.copy_sources['orga'])

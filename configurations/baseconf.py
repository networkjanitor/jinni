import csv


class BaseSwitch:
    tabs = []

    def get_tabinfos(self):
        return self.tabs

    def get_tab(self, name):
        for tab in self.tabs:
            if tab.name == name:
                return tab
        return BaseConf()


class BaseConf:
    name = "BaseConf"

    def __init__(self):
        self.choices = {}
        self.ordered_choices = []

    def get_data(self):
        return self.choices, self.ordered_choices

    def _add_empty_line(self):
        self.choices[''] = ''
        self.ordered_choices.append('')

    def _load_csv(self, file_path, data_mod_func=None, data_sort_func=None):
        if data_mod_func is None:
            data_mod_func = self._data_modifier

        if data_sort_func is None:
            data_sort_func = self._data_sort

        with open(file_path, 'r') as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=';', quotechar='"')
            for row in csv_reader:
                new_rows = data_mod_func(row['title'], row['text'])
                for data_touple in new_rows:
                    self.ordered_choices.append(data_touple['title'])
                    self.choices[data_touple['title']] = {}
                    self.choices[data_touple['title']]['text'] = data_touple['text']

        data_sort_func(self.ordered_choices)


    @staticmethod
    def _data_modifier(key, value):
        return [{'title': key, 'text': value}]

    @staticmethod
    def _data_sort(choices_list):
        return choices_list



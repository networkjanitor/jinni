from configurations.baseconf import BaseConf
from settings import Settings

class Conf(BaseConf):
    name = 'OC'

    def __init__(self):
        super().__init__()
        self._load_csv(Settings.copy_sources['oc-links'])


from configurations.baseconf import BaseConf
from settings import Settings

import configurations.opencommunity as occonf
import configurations.orga as orgaconf
from configurations.baseconf import BaseConf, BaseSwitch
from settings import Settings


class Switch(BaseSwitch):
    def __init__(self):
        self.tabs.append(Conf())
        self.tabs.append(orgaconf.Conf())
        self.tabs.append(occonf.Conf())


class Conf(BaseConf):
    name = 'Shatterer'

    def __init__(self):
        super().__init__()
        self._load_csv(Settings.copy_sources['shatterer'])


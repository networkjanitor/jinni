import canni

if __name__ == '__main__':
    shg = canni.StaticHtmlGenerator(output_directory='gitlab-pages')
    shg.render_switch('shatterer', output_filename='shatterer.html',
                      pagetitle='The Shatterer')

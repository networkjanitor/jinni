class Settings:
    keys = {
        'select': ('return', 'enter'),
        'escape': ('esc', 'q', 'Q'),
        'up': ('up', 'k'),
        'down': ('down', 'j'),
        'home': ('0', '^'),
        'end': ('$',),
    }

    copy_sources = {
        'oc-links': './copy_sources/opencommunity_links.csv',
        'shatterer': './copy_sources/shatterer.csv',
        'orga': './copy_sources/orga.csv',

    }

    scroll_scaling = {
        'wheel-up': ['up' for x in range(5)],
        'wheel-down': ['down' for x in range(5)],
        'key-up': ['up'],
        'key-down': ['down'],
    }